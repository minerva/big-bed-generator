Mapping bb files are located on minerva-net: /var/www/genomics/

Available by url:

- https://minerva-net.lcsb.uni.lu/genomics/refGene38.bb
- https://minerva-net.lcsb.uni.lu/genomics/refGene19.bb
- https://minerva-net.lcsb.uni.lu/genomics/refGene18.bb
