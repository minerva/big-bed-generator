[![pipeline status](https://git-r3lab.uni.lu/piotr.gawron/big-bed-generator/badges/master/pipeline.svg)](https://git-r3lab.uni.lu/piotr.gawron/big-bed-generator/commits/master)

# Introduction

This project aim is to provide simple tool that creates [BigBed](https://genome.ucsc.edu/goldenpath/help/bigBed.html) file from gene definition downloadable from [UCSC](http://hgdownload.cse.ucsc.edu/downloads.html). The script is designed to be run on linux and uses heavily tools provided by UCSC: http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/

# Usage


Download definition of genes. For instance to download refGene definition for human hg38 reference genome execute:

```bash
curl https://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/refGene.txt.gz -o refGene.txt.gz
```

Now you can call the script providing as an input file with genes and identifier of the reference genome:

```bash
python3 generate.py refGene-19.txt.gz hg19
```

This should generate file called `refGene-19.bb`

There is also `--skipNonStandardChromosomes` parameter that skip all chromosomes (contigs) that contain "_" character and are not considered as standard chromosomes (chr1, chr2, etc.).
Here is a sample usage:
```bash
python3 generate.py refGene.txt.gz hg38 --skipNonStandardChromosomes
```